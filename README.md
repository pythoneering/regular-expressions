[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/pythoneering%2Fregular-expressions/master)

### Adding branches

When making a branch following a naming convention aids in bookeeping.
The following convention I have found useful:

\<type\>\_\<user\>\_\<changesBeingMade\>

where:

\<type\> = ftr (feature), bug, iss (issue), etc.

\<user\> = atrahan

\<changesBeingMade\> = addExamples

e.g.

ftr_atrahan_addExamples or iss01_atrahan_finishQuantifiers


### Markdown Cheatsheet

[Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#lists)
